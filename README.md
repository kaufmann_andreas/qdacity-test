# QDAcity Test



## About QDAcity

QDAcity is a [qda software](https://qdacity.com/qda-software/) used for [qualitative surveys](https://qdacity.com/qualitative-survey/), [thematic analysis](https://qdacity.com/thematic-analyis/) and [grounded theory](https://qdacity.com/grounded-theory/)
